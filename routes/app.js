const express = require('express');
const app = express()

require('./routes')(app);
console.log('loading routes...');

app.listen(3000, ()=> {
  console.log('Example app listenning on port 3000');
})
